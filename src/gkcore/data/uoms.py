""" List of UQC with unit names.
"""

UQC_LIST = {
    "BAG": "BAGS",
    "BAL": "BALE",
    "BDL": "BUNDLES",
    "BKL": "BUCKLES",
    "BOU": "BILLIONS OF UNITS",
    "BOX": "BOX",
    "BTL": "BOTTLES",
    "BUN": "BUNCHES",
    "CAN": "CANS",
    "CBM": "CUBIC METER",
    "CCM": "CUBIC CENTIMETER",
    "CMS": "CENTIMETER",
    "CRT": "Carat",
    "CTN": "CARTONS",
    "DOZ": "DOZEN",
    "DRM": "DRUM",
    "GGK": "GREAT GROSS",
    "GMS": "GRAMS",
    "GRS": "GROSS",
    "GYD": "GROSS YARDS",
    "KGS": "KILOGRAMS",
    "KLR": "KILOLITER",
    "KME": "KILOMETERS",
    "MLT": "MILLILITER",
    "MTR": "METER",
    "MTS": "METRIC TON",
    "NOS": "NUMBER",
    "OTH": "OTHERS",
    "PAC": "PACKS",
    "PCS": "PIECES",
    "PRS": "PAIRS",
    "QTL": "QUINTAL",
    "ROL": "ROLLS",
    "SET": "SETS",
    "SQF": "SQUARE FEET",
    "SQM": "SQUARE METER",
    "SQY": "SQUARE YARDS",
    "TBS": "TABLETS",
    "TGM": "TEN GRAMS",
    "THD": "THOUSANDS",
    "TON": "GREAT BRITAIN TON",
    "TUB": "TUBES",
    "UGS": "US GALLONS",
    "UNT": "UNITS",
    "YDS": "YARDS",
}
